//
//  SqliteDatabase.h
//  SqliteObjectivecUtils
//
//  Created by Hung Truong on 7/25/19.
//

#import <Foundation/Foundation.h>
#import <FMDB/FMDB.h>

NS_ASSUME_NONNULL_BEGIN

@interface SqliteDatabase : NSObject {
    NSString *_path;
}

@property (nonatomic, readonly) NSString *path;

- (instancetype)initWithDatabasePath:(NSString *)databasePath schema:(NSString *)schema migrationsBundle:(NSBundle *)migrationsBundle;

- (void)close;

@end

@protocol FMDBMigrating;

/**
 @abstract The `FMDBMigrationManager` class provides a simple, flexible interface for managing migrations for a SQLite database that is accessed via FMDB.
 */
@interface FMDBMigrations : NSObject

/**
 @abstract Creates a new migration manager with a given database and migrations bundle.
 @param database The database with which to initialize the migration manager.
 @param bundle The bundle containing the migrations.
 @return A new migration manager.
 */
+ (instancetype)managerWithDatabase:(FMDatabase *)database migrationsBundle:(NSBundle *)bundle;

/**
 @abstract Creates a new migration manager with a database for the given database and migrations bundle.
 @param path The path to a database with which to initialize the migration manager.
 @param bundle The bundle containing the migrations.
 @return A new migration manager.
 */
+ (instancetype)managerWithDatabaseAtPath:(NSString *)path migrationsBundle:(NSBundle *)bundle;

/**
 @abstract Determines whether the receiver will perform a search for dynamically defined migrations. Default: `YES`.
 */
@property (nonatomic, assign) BOOL dynamicMigrationsEnabled;

/**
 @abstract Returns the database of the receiver.
 */
@property (nonatomic, readonly) FMDatabase *database;

/**
 @abstract Returns the migrations bundle for the receiver.
 */
@property (nonatomic, readonly) NSBundle *migrationsBundle;

/**
 @abstract Returns the current version of the database managed by the receiver or `0` if the migrations table is not present.
 */
@property (nonatomic, readonly) uint64_t currentVersion;

/**
 @abstract Returns the origin version of the database managed by the receiver or `0` if the migrations table is not present.
 */
@property (nonatomic, readonly) uint64_t originVersion;

/**
 @abstract Returns all migrations discovered by the receiver. Each object returned conforms to the `FMDBMigrating` protocol. The array is returned in ascending order by version.
 */
@property (nonatomic, readonly) NSArray *migrations;

/**
 @abstract Returns the version numbers of the subset of `migrations` that have already been applied to the database managed by the receiver in ascending order.
 */
@property (nonatomic, readonly) NSArray *appliedVersions;

/**
 @abstract Returns the version numbers of the subset of `migrations` that have not yet been applied to the database
 managed by the receiver in ascending order.
 */
@property (nonatomic, readonly) NSArray *pendingVersions;

/**
 @abstract Returns a migration object with a given version number or `nil` if none could be found.
 @param version The version of the desired migration.
 @return A migration with the specified version or `nil` if none could be found.
 */
- (id<FMDBMigrating>)migrationForVersion:(uint64_t)version;

/**
 @abstract Returns a migration object with a given name or `nil` if none could be found.
 @param name The name of the desired migration.
 @return A migration with the specified named or `nil` if none could be found.
 */
- (id<FMDBMigrating>)migrationForName:(NSString *)name;

/**
 @abstract Adds a migration to the receiver's list.
 @param migration The migration to add.
 */
- (void)addMigration:(id<FMDBMigrating>)migration;

/**
 @abstract Adds migrations from the array to the receiver's list.
 */
- (void)addMigrations:(NSArray *)migrations;

/**
 @abstract Returns a Boolean value that indicates if the `schema_migrations` table
 is present in the database.
 */
@property (nonatomic, readonly) BOOL hasMigrationsTable;

/**
 @abstract Creates the `schema_migrations` table used by `FMDBMigrationManager` to maintain an index of applied migrations.
 */
- (BOOL)createMigrationsTable:(NSError **)error;

/**
 @abstract Returns a Boolean value that indicates if the database managed by the receiver is in need of migration.
 */
@property (nonatomic, readonly) BOOL needsMigration;

/**
 @abstract Migrates the database managed by the receiver to the specified version, optionally providing progress via a block.
 */
- (BOOL)migrateDatabaseToVersion:(uint64_t)version progress:(void (^)(NSProgress *progress))progressBlock error:(NSError **)error;

@end

@protocol FMDBMigrating <NSObject>

/**
 @abstract The name of the migration.
 */
@property (nonatomic, readonly) NSString *name;

/**
 @abstract The numeric version of the migration.
 */
@property (nonatomic, readonly) uint64_t version;

/**
 @abstract Tells the receiver to apply its changes to the given database and return a Boolean value indicating success or failure.
 @param database The database on which to apply the migration.
 @param error A pointer to an error object to set should the transaction fail.
 @return A Boolean value indicating if the
 */
- (BOOL)migrateDatabase:(FMDatabase *)database error:(out NSError *__autoreleasing *)error;

@end

@interface FMDBFileMigration : NSObject <FMDBMigrating>

/**
 @abstract Creates and returns a new migration with the file at the given path.
 @param path The path to a file containing a SQL migration with a conformant filename.
 */
+ (instancetype)migrationWithPath:(NSString *)path;

/**
 @abstract The path to the SQL migration file on disk.
 */
@property (nonatomic, readonly) NSString *path;

/**
 @abstract A convenience accessor for retrieving the SQL from the receiver's path.
 */
@property (nonatomic, readonly) NSString *SQL;

@end

extern NSString *const FMDBMigrationManagerErrorDomain;
extern NSString *const FMDBMigrationManagerProgressVersionUserInfoKey;
extern NSString *const FMDBMigrationManagerProgressMigrationUserInfoKey;

BOOL FMDBIsMigrationAtPath(NSString *path);

/**
 @abstract Enumerates the errors returned by FMDBMigrations
 */
typedef NS_ENUM(NSUInteger, FMDBMigrationManagerError) {
    /// Indicates that migration was halted due to cancellation
    FMDBMigrationManagerErrorMigrationCancelled  = 1
};


NS_ASSUME_NONNULL_END
