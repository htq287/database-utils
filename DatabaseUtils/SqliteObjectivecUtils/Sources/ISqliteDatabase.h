//
//  ISqliteDatabase.h
//  SqliteObjectivecUtils
//
//  Created by Hung Truong on 7/25/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ISqliteDatabase <NSObject>

- (void)addObject:(id)object;
- (void)updateObject:(id)object;
- (void)deleteObject:(id)object;

@end

NS_ASSUME_NONNULL_END
