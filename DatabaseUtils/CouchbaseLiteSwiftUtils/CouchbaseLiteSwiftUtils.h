//
//  CouchbaseLiteSwiftUtils.h
//  CouchbaseLiteSwiftUtils
//
//  Created by Hung Truong on 7/25/19.
//

#import <Cocoa/Cocoa.h>

//! Project version number for CouchbaseLiteSwiftUtils.
FOUNDATION_EXPORT double CouchbaseLiteSwiftUtilsVersionNumber;

//! Project version string for CouchbaseLiteSwiftUtils.
FOUNDATION_EXPORT const unsigned char CouchbaseLiteSwiftUtilsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CouchbaseLiteSwiftUtils/PublicHeader.h>


