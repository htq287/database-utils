//
//  SqliteDatabaseTests.m
//  SqliteObjectivecUtilsTests
//
//  Created by Hung Truong on 7/25/19.
//

#import <XCTest/XCTest.h>
#import "SqliteDatabaseTestCase.h"

@interface SqliteDatabaseTests : SqliteDatabaseTestCase

@property (nonatomic, strong) SqliteDatabaseTestCase *dbTestCase;

@end

@implementation SqliteDatabaseTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
    [super setUp];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

#pragma mark - Create Database

- (void)testCreateDatabase {
    [self createDatabase];
    [self deleteDatabase:self.db];
}

@end
