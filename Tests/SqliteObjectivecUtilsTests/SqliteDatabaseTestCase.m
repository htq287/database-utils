//
//  SqliteDatabaseTestCase.m
//  SqliteObjectivecUtilsTests
//
//  Created by Hung Truong on 7/25/19.
//

#import "SqliteDatabaseTestCase.h"

@implementation SqliteDatabaseTestCase {
    NSString *_dbPath;
    NSString *_dbSchema;
    NSBundle *_migrationsBundle;
}

@synthesize db = _db;

- (instancetype)init {
    if(self = [super init]) {
        [self setUp];
    }
    
    return self;
}

- (void)setUp {
    [super setUp];
    
    NSBundle *testsBundle = [NSBundle bundleForClass:[self class]];
    AssertNotNil(testsBundle);
    
    NSDictionary *infoDict = [testsBundle infoDictionary];
    AssertNotNil(infoDict);
    
    NSString *dbName = infoDict[@"db_name"];
    NSString *testsBundleId = infoDict[@"test_bundle_identifier"];
    
    AssertNotNil(dbName);
    AssertNotNil(testsBundleId);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *folder = [paths objectAtIndex:0];
    folder = [folder stringByAppendingPathComponent:testsBundleId];
    [[NSFileManager defaultManager] createDirectoryAtPath:folder withIntermediateDirectories:YES attributes:nil error:NULL];
    
    AssertNotNil(folder);
    _dbPath = [folder stringByAppendingPathComponent:dbName];
    
    NSString *schemaPath = [testsBundle pathForResource:@"schema_tests" ofType:@"db"];
    AssertNotNil(schemaPath);
    NSError *error;
    _dbSchema = [NSString stringWithContentsOfFile:schemaPath encoding:NSUTF8StringEncoding error:&error];
    AssertNotNil(_dbSchema);
    
    _migrationsBundle = [NSBundle bundleWithPath:[testsBundle pathForResource:@"Migrations" ofType:@"bundle"]];
    AssertNotNil(_migrationsBundle);
    
}

- (void)createDatabase {
    _db = [self openWithDatabasePath:_dbPath schema:_dbSchema migrationsBundle:_migrationsBundle];
    
    Assert([[NSFileManager defaultManager] fileExistsAtPath:_db.path]);
}

- (SqliteDatabase *)openWithDatabasePath:(NSString *)dbPath schema:(NSString *)schema migrationsBundle:(NSBundle *)bundle {
    return [[SqliteDatabase alloc] initWithDatabasePath:dbPath schema:schema migrationsBundle:bundle];
}

- (void)deleteDatabase:(SqliteDatabase *)db {
    assert(db != nil);
    [db close];
    NSString *path = db.path;
    Assert([[NSFileManager defaultManager] fileExistsAtPath:path]);
    
    NSError *removeError;
    [[NSFileManager defaultManager] removeItemAtPath:path error:&removeError];
    AssertNil(removeError);
    AssertFalse([[NSFileManager defaultManager] fileExistsAtPath:path]);
}

@end
