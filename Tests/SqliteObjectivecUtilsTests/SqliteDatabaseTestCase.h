//
//  SqliteDatabaseTestCase.h
//  SqliteObjectivecUtilsTests
//
//  Created by Hung Truong on 7/25/19.
//

#import <Foundation/Foundation.h>
#import <XCTest/XCTest.h>
#import <SqliteObjectivecUtils/SqliteObjectivecUtils.h>

#define Assert             XCTAssert
#define AssertNil          XCTAssertNil
#define AssertNotNil       XCTAssertNotNil
#define AssertEqual        XCTAssertEqual
#define AssertEqualObjects XCTAssertEqualObjects
#define AssertFalse        XCTAssertFalse

NS_ASSUME_NONNULL_BEGIN

@interface SqliteDatabaseTestCase : XCTestCase {
@protected
    SqliteDatabase* _db;
}

@property (nonatomic, readonly) SqliteDatabase *db;

- ( SqliteDatabase * _Nullable )openWithDatabasePath:(NSString *)dbPath schema:(NSString *)schema migrationsBundle:(NSBundle *)bundle;

- (void)createDatabase;
- (void)deleteDatabase:( SqliteDatabase * _Nonnull )db;

@end

NS_ASSUME_NONNULL_END
