# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.0.1] - 2019-07-24
### Added
- DatabaseUtils
  - add RealmSwiftUtils
  - add CouchbaseLiteSwiftUtils
  - add SqliteObjectivecUtils
- Samples
  - SampleObjectivec
  - SampleSwift
- add README.md
- add CHANGELOG.md
- add MIT LICENSE
- manage Dependencies with Cocoapod
  - create Podfile
- xcodegen configs
  - add project.yml
  - add .xcconfig files
- add Scripts
  - auto configurate project with Cocoapod
- add Makefile