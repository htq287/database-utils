SHELL := /bin/bash
CWD := $(shell cd -P -- '$(shell dirname -- "$0")' && pwd -P)
SCRIPTS_DIR := $(CWD)/scripts

BUILD_DIR		:= ./build

# sdks
TARGET_SDK		:= macosx

print:
	@echo $(CWD)
	@echo $(SCRIPTS_DIR)

initproj:
	sh $(SCRIPTS_DIR)/pods-init-project.sh

all: clean build

clean:
	-@rm -rfv $(BUILD_DIR)

.PHONY: all clean