#!/bin/sh -e

# remove the old pods-configs
rm -rf DatabaseUtils.*
rm -rf Podfile.lock
rm -rf Pods

# generate .xcodeproj
xcodegen

# regenerate .xcworkspace
pod install