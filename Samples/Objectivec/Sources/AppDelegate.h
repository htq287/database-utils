//
//  AppDelegate.h
//  SampleObjectivec
//
//  Created by Hung Truong on 7/24/19.
//  Copyright © 2019 Hung Truong. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

