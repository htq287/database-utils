//
//  AppDelegate.m
//  SampleObjectivec
//
//  Created by Hung Truong on 7/24/19.
//  Copyright © 2019 Hung Truong. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
