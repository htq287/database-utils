//
//  main.m
//  SampleObjectivec
//
//  Created by Hung Truong on 7/24/19.
//  Copyright © 2019 Hung Truong. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
